// Definice připojení pinů
int light = 0;        // LED pouličního osvětlení
int button = 2;       // Tlačítko pro změnu světel na semaforech
int yellow1 = 7;      // Žlutá LED pro semafor 1
int yellow2 = 6;      // Žlutá LED pro semafor 2
int traffic1 = 8;     // Červená/Zelená LED pro semafor 1
int traffic2 = 5;     // Červená/Zelená LED pro semafor 2
int photo = A0;       // Fotorezistor pro detekci úrovně světla

// Nastavení režimu pinů
void setup() {
  pinMode(light, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  pinMode(yellow1, OUTPUT);
  pinMode(yellow2, OUTPUT);
  pinMode(traffic1, OUTPUT);
  pinMode(traffic2, OUTPUT);
  pinMode(photo, INPUT);
}

// Hlavní smyčka programu
void loop() {
  int lightLevel = analogRead(photo); // Čtení úrovně světla
  static bool trafficState = false;   // Aktuální stav světelné signalizace
  static unsigned long lastChange = 0; // Poslední změna světel

  // Pokud je tma, zapněte pouliční světla a nastavte semafory na blikající žlutou
  if (lightLevel < 500) {
    digitalWrite(light, HIGH); // Zapnutí pouličního světla
    // Vypnutí semaforů
    digitalWrite(traffic1, LOW);
    digitalWrite(traffic2, LOW);
    // Blikání žlutých světel
    digitalWrite(yellow1, HIGH);
    digitalWrite(yellow2, HIGH);
    delay(2000);
    digitalWrite(yellow1, LOW);
    digitalWrite(yellow2, LOW);
    delay(2000);
  } else {
    // Pokud není tma, vypněte pouliční světlo
    digitalWrite(light, LOW);
    // Pokud je stisknuto tlačítko a od poslední změny uplynulo více než 5 sekund
    if (digitalRead(button) == LOW && millis() - lastChange > 5000) {
      // Změna stavu semaforu
      if (trafficState) {
        // Pokud je semafor 1 zelený, připravte se na změnu na červenou
        digitalWrite(yellow1, HIGH);
        delay(2000);
        digitalWrite(yellow1, LOW);
      } else {
        // Pokud je semafor 2 zelený, připravte se na změnu na červenou
        digitalWrite(yellow2, HIGH);
        delay(2000);
        digitalWrite(yellow2, LOW);
      }
      trafficState = !trafficState; // Přepnutí stavu
      lastChange = millis(); // Aktualizace času změny
    }
    // Nastavení semaforů podle aktuálního stavu
    if (trafficState) {
      digitalWrite(traffic1, HIGH); // Semafor 1 je zelený
      digitalWrite(traffic2, LOW);  // Semafor 2 je červený
    } else {
      digitalWrite(traffic1, LOW);  // Semafor 1 je červený
      digitalWrite(traffic2, HIGH); // Semafor 2 je zelený
    }
  }
}
