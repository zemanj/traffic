# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 20 hodin |
| jak se mi to podařilo rozplánovat | Ne úplně nejlépe |
| návrh designu | https://gitlab.spseplzen.cz/zemanj/traffic/-/blob/master/dokumentace/design/navrh/Traffic.jpg |
| proč jsem zvolil tento design | Je to podle reálné křižovatky, přišel mi zajímavý, ale nestihl jsem vypracovat vše co jsem tam chtěl. |
| zapojení | https://gitlab.spseplzen.cz/zemanj/traffic/-/blob/master/dokumentace/design/Traffic.png |
| z jakých součástí se zapojení skládá | fotorezistor, LED, rezistor 220 ohmový a 10K ohmový, hodně dílů na 3d tiskárně, kabely, deska na shield |
| realizace | https://gitlab.spseplzen.cz/zemanj/traffic/-/blob/master/dokumentace/fotky/IMG20240410203253.jpg |
| jaký materiál jsem použil a proč | plast a dřevo, protože plast tiskne 3d tiskárna a se dřevem se jednoduše pracuje |
| co se mi povedlo | vše co není připojeno k arduinu |
| co se mi nepovedlo/příště bych udělal/a jinak | vše co je připojeno k arduinu |
| zhodnocení celé tvorby | Zajímový projekt provedení zase až tak moc ne. Měl jsem velké očekávání a celý to dopadlo tak nějak MEH |